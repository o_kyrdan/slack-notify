# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.3

- patch: Fix string interpolation inside the jq command used to prepare the message sent to slack.

## 0.2.2

- patch: Fix escaping variables with double quotes.

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming from task to pipe. Add the pipe.yml descriptor.

## 0.1.1

- patch: Improve task logging

## 0.1.0

- minor: Initial version

