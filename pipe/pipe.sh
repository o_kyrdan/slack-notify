#!/usr/bin/env bash
#
# Send a notification to Slack, https://api.slack.com/docs/messages
#
# Required globals:
#  WEBHOOK_TOKEN
#  MESSAGE

source "$(dirname "$0")/common.sh"

enable_debug
extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

# mandatory variables
WEBHOOK_URL=${WEBHOOK_URL:?'WEBHOOK_URL variable missing.'}
MESSAGE=${MESSAGE:?'MESSAGE variable missing.'}

info "Sending notification to Slack..."

output_file="/tmp/pipe-$RANDOM.txt"

payload=$(jq -n \
  --arg MESSAGE "${MESSAGE}" \
  --arg BITBUCKET_REPO_OWNER "${BITBUCKET_REPO_OWNER}" \
  --arg BITBUCKET_REPO_SLUG "${BITBUCKET_REPO_SLUG}" \
  --arg BITBUCKET_BUILD_NUMBER "${BITBUCKET_BUILD_NUMBER}" \
'{ attachments: [
  {
    "fallback": $MESSAGE,
    "color": "#439FE0",
    "pretext": "Notification sent from <https://bitbucket.org/\($BITBUCKET_REPO_OWNER)/\($BITBUCKET_REPO_SLUG)/addon/pipelines/home#!/results/\($BITBUCKET_BUILD_NUMBER)|Pipeline #\($BITBUCKET_BUILD_NUMBER)>",
    "text": $MESSAGE,
    "mrkdwn_in": ["pretext"]
  }
]}')

run curl -s -X POST --output $output_file -w "%{http_code}" \
  -H "Content-Type: application/json" \
  -d "${payload}" \
  ${extra_args} \
  $WEBHOOK_URL

response=$(cat $output_file)
info "HTTP Response: $(echo $response)"

if [[ "${response}" = "ok" ]]; then
  success "Notification successful."
else
  fail "Notification failed."
fi
